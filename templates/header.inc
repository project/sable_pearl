<div class="toggle-control-open toggle-control"><span class="glyphicon circlePlus"></span></div>
<div class="content_highlight wrapper-box">     
    <div class="container">
      <?php print render($page['post_content_bg']); ?>
    </div>
   <div class="toggle-control-close"><span class="glyphicon glyphicon-minus"></span></div>
 </div>
<header id="navbar" role="banner" class="navbar navbar-default inner-page-header">
   <div class="container-fluid">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle-open">         
          <i class="fa fa-bars"></i>
         </button>         
         <?php if ($logo): ?>
         <a class="logo navbar-btn" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
         <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
         </a>
         <?php endif; ?>
        <!--  <?php if (!empty($site_name)): ?>
         <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
         <?php endif; ?>   -->       
      </div>     
   </div>
</header>
<div class="sidebar-navber">
  <button type="button" class="navbar-toggle-close">
    <img class="img-responsive" src="/sites/all/themes/corporate/images/closeBtn.png">
  </button>
 <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
         <nav role="navigation">
            <?php if (!empty($primary_nav)): ?>
               <?php print render($primary_nav); ?>
               <?php endif; ?>
            <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
            <?php endif; ?>
         </nav>
      <?php endif; ?>

</div>